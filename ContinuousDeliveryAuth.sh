#!/bin/sh

#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan remote add "bimandscan-public" $ARTIFACTORY_REMOTE_BIMANDSCAN_PUBLIC
conan remote add "bintray-cache" $ARTIFACTORY_REMOTE_BIMANDSCAN_BINTRAY_CACHE --insert 0
conan user -p $ARTIFACTORY_REMOTE_BIMANDSCAN_KEY -r "bimandscan-public" $ARTIFACTORY_REMOTE_BIMANDSCAN_USER
