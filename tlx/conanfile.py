#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class TLX(ConanFile):
    name = "tlx"
    version = "20181204"
    license = "BSL-1.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-tlx"
    description = "A collection of sophisticated C++ data structures, algorithms, and miscellaneous helpers."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://tlx.github.io/"

    _src_dir = "tlx_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        git_uri = "https://github.com/tlx/tlx.git"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "ffb45bf5222946bc499cf5a737ae9b01e4427d00") # commit hash -> 4/12/2018

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "project(tlx)",
                              "project(tlx CXX)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup()")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["TLX_BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["TLX_BUILD_STATIC_LIBS"] = not self.options.shared
        cmake.definitions["TLX_MORE_TESTS"] = False
        cmake.definitions["TLX_BUILD_TESTS"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
