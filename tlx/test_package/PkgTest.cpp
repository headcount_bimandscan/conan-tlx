/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <tlx/thread_pool.hpp>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'TLX' package test (compilation, linking, and execution).\n";
    tlx::ThreadPool pool(2);

    pool.enqueue([]() -> void
                 {
                     int temp1 = 1,
                         temp2 = 2;

                     const int temp3 = temp1 + temp2;
                 });

    pool.loop_until_empty();

    std::cout << "'TLX' package works!" << std::endl;
    return EXIT_SUCCESS;
}
