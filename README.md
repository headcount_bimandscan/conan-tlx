# BIM & Scan® Third-Party Library (TLX)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [TLX](https://tlx.github.io/), a collection of sophisticated C++ data structures, algorithms, and miscellaneous helpers.

Requires a C++11 compiler (or higher).

Supports snapshot 4/12/2018 (unstable, from Git repo).
